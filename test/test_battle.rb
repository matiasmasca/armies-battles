require 'minitest/autorun'
require 'minitest/pride'
require 'minitest/hell'

# require "minitest/reporters"
# Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require './src/civilization'
require './src/player'
require './src/battle_history'
require './src/unit'
require './src/pikeman'
require './src/archer'
require './src/knight'
require './src/barrack'
require './src/army'
require './src/battle'

describe Battle do
  before do
    @player_one = Player.new(civilization: "Byzantine")
    @player_one.new_army
    @player_one_army = @player_one.armies.first

    @player_two = Player.new(civilization: "English")
    @player_two.new_army
    @player_two_army = @player_two.armies.first
  end

  describe 'for armies without improvements' do
    before do
      @byzantine_player = Player.new(civilization: "Byzantine")
      @byzantine_player.new_army
      @byzantine_army = @byzantine_player.armies.first

      @english_player = Player.new(civilization: "English")
      @english_player.new_army
      @english_army = @english_player.armies.first

      @chinese_player = Player.new(civilization: "Chinese")
      @chinese_player.new_army
      @chinese_army = @chinese_player.armies.first
    end

    describe 'When a Byzantine attack a Chinese army' do
      it 'should win' do
        expect( @byzantine_army.attack(@chinese_army) ).must_equal :winner
      end
    end

    describe 'When a Byzantine attack a English army' do
      it 'should win' do
        expect( @byzantine_army.attack(@english_army) ).must_equal :winner
      end
    end

    describe 'When a English attack a Chinese army' do
      it 'should win' do
        expect( @english_army.attack(@chinese_army) ).must_equal :winner
      end
    end

    describe 'When a Chinese attack a English army' do
      it 'should lose' do
        expect( @chinese_army.attack(@english_army) ).must_equal :losser
      end
    end
  end

  describe 'Battle consecuences' do
    describe 'When an army lose a battle' do
      it 'should lose 2 units' do
        before_units = @player_two_army.units.count
        expect(@player_two_army.attack(@player_one_army)).must_equal :losser
        expect(@player_two_army.units.count).must_be :==, before_units - 2
      end

      it 'should lose the 2 strongest units' do
        @temp_army = @player_one_army
        strongets_points = 0
        2.times do | unit |
          strongets_points += @temp_army.strongest_unit.strength_points
          @temp_army.destroy_unit @temp_army.strongest_unit
        end

        before_attack_points = @player_two_army.attack_points

        expect(@player_two_army.attack(@player_one_army)).must_equal :losser
        expect(@player_two_army.attack_points).must_be :==, before_attack_points - strongets_points
      end
    end

    describe 'When an Army win a battle' do
      it 'should increse his treasure in 100 gold coins' do
        before_gold_coins = @player_one_army.gold_coins
        expect( @player_one_army.attack(@player_two_army) ).must_equal :winner
        expect(@player_one_army.gold_coins).must_be :>, before_gold_coins
      end
    end

    describe 'When a battle ends in a draw' do
      before do
        @xin = Player.new(civilization: "Chinese")
        @xin.new_army
        @xin_army = @xin.armies.first

        @yang = Player.new(civilization: "Chinese")
        @yang.new_army
        @yang_army = @yang.armies.first
      end

      it 'then both players should lose their strongest unit' do
        before_attack_points_player_xin = @xin_army.attack_points
        before_attack_points_player_yang = @yang_army.attack_points

        expect( @xin_army.attack(@yang_army) ).must_equal :tied

        expect(@xin_army.attack_points).must_be :<, before_attack_points_player_xin
        expect(@yang_army.attack_points).must_be :<, before_attack_points_player_yang

        expect(@xin_army.attack_points).must_be :==, 280
        expect(@yang_army.attack_points).must_be :==, 280
      end
    end

  end
end

