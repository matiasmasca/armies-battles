
class Knight < Unit
  AGREED_POINTS = 20

  def initialize(args = {})
    @convertible = false
    @strength_points = args[:point] || AGREED_POINTS
  end

end
