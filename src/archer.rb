
class Archer < Unit
  AGREED_POINTS = 10

  def initialize(args = {})
    @convertible = true
    @strength_points = args[:point] || AGREED_POINTS
  end

end
