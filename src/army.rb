
class Army
  attr_reader :gold_coins, :units, :battle_history, :civilization, :strongest_unit

  def initialize(civilization = Civilization.new)
    @gold_coins = 1000
    @civilization = civilization
    @units = []

    @battle_history = BattleHistory.new

    set_intial_units
  end

  def attack_points
    @units.reduce(0) { |sum, unit| sum + unit.strength_points }
  end

  def strongest_unit
     strongest = @units.inject do |strongest_unit, unit|
      strongest_unit.strength_points >= unit.strength_points ? strongest_unit : unit
     end
  end

  def train(unit)
    Barrack.train(unit, self)
  end

  def convert(unit)
    Barrack.convert(unit, self)
  end

  def attack(enemy_army)
    battle = Battle.new(attacking_army: self, defense_army: enemy_army)
    battle.start
  end

  def add_unit(unit)
    @units << unit
  end

  def destroy_unit(unit)
    @units.delete(unit)
    unit = nil
  end

  def pay_gold(amount)
    @gold_coins -= amount
  end

  def receive_gold(amount)
    @gold_coins += amount
  end

  private

  def set_intial_units
    Barrack.create_initial_units_army(self)
  end
end
